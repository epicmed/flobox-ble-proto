import React from 'react';
import type {Node} from 'react';
import {
  Button,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';

import {Colors, Header} from 'react-native/Libraries/NewAppScreen';
import {Notifications} from 'react-native-notifications';
import BleService from './src/BleService';
import BackgroundFetch from 'react-native-background-fetch';

const Section = ({children, title}): Node => {
  const isDarkMode = useColorScheme() === 'dark';
  return (
    <View style={styles.sectionContainer}>
      <Text
        style={[
          styles.sectionTitle,
          {
            color: isDarkMode ? Colors.white : Colors.black,
          },
        ]}>
        {title}
      </Text>
      <Text
        style={[
          styles.sectionDescription,
          {
            color: isDarkMode ? Colors.light : Colors.dark,
          },
        ]}>
        {children}
      </Text>
    </View>
  );
};

const App: () => Node = () => {
  Notifications.registerRemoteNotifications();
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  return (
    <SafeAreaView style={backgroundStyle}>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={backgroundStyle}>
        <Header />
        <View
          style={{
            backgroundColor: isDarkMode ? Colors.black : Colors.white,
          }}>
          <Section title="Local Notification">
            <Button title="Try Me" onPress={notificationOnPress} />
          </Section>
          <Section title="Bluetooth Scanning">
            <Button title="Start" onPress={scanOnPress} />
          </Section>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const notificationOnPress = () => {
  // console.log(
  //   'Launch Local',
  //   Buffer.from('3gdquGCRAAMAAgcD6GQ=', 'base64').readUInt16LE(0),
  // );
  // setTimeout(createNotification, 5000);
};

const scanOnPress = () => {
  const bleService = new BleService();
  bleService.startScanning();
};

const createNotification = () => {
  Notifications.postLocalNotification({
    body: 'Your infusion is about to complete!',
    title: 'eZS Pump',
    silent: false,
    category: 'PUMP_UPDATE',
    userInfo: {},
  });
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
