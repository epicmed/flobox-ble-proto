import {BleManager} from 'react-native-ble-plx';
import {Notifications} from 'react-native-notifications';
let instance = null;
let previousData = null;

class BleService {
  constructor(dispatcher) {
    if (!instance) {
      instance = new BleManager({
        restoreStateIdentifier: 'BleInTheBackground',
        restoreStateFunction: restoredState => {
          if (restoredState == null) {
            // BleManager was constructed for the first time.
            console.log('first time');
          } else {
            // BleManager was restored. Check `restoredState.connectedPeripherals` property.
            console.log('restore');
            console.log(restoredState);
          }
        },
      });
    }
    this.dispatcher = dispatcher;
  }

  async checkStatus() {
    return await instance.state();
  }

  async startScanning() {
    instance.onStateChange(state => {
      if (state === 'PoweredOn') {
        this.scan();
      }
    });
  }

  scan() {
    setInterval(() => {
      console.log('Start Scanning');
      this.scanForDevices();
    }, 20000);
  }

  scanForDevices() {
    instance.startDeviceScan(
      ['00001803-0000-1000-8000-00805f9b34fb'],
      {allowDuplicates: false},
      (error, device) => {
        if (error) {
          // Handle error (scanning will be stopped automatically)
          console.log('Device error:', error);
          return;
        }
        console.log(device);
        if (device.id === '742B3C22-C950-C7B8-E142-F874C8DDCE1B') {
          const serviceData =
            device.serviceData['00001803-0000-1000-8000-00805f9b34fb'];
          console.log('Device 3', serviceData);
          Notifications.postLocalNotification({
            body: 'Device 3',
            title: 'eZS Pump',
            silent: false,
            category: 'PUMP_UPDATE',
            userInfo: {},
          });
          if (!previousData) {
            previousData = serviceData;
          } else if (previousData !== serviceData) {
            console.log('data changed!!!');
            Notifications.postLocalNotification({
              body: 'DAta Changed!',
              title: 'eZS Pump',
              silent: false,
              category: 'PUMP_UPDATE',
              userInfo: {},
            });
          }
        }
      },
    );
    setTimeout(() => {
      instance.stopDeviceScan();
    }, 5000);
  }
}

export default BleService;
